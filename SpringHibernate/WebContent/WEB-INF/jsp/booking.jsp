<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>Booking </title>
</head>

	<h2>Please confirm your booking</h2>
<form:form action="add.htm" commandName="booking">
	<table>
		<tr>
			<td>event_id:</td>
			<td><form:input path="event_id" value="${eventid}" /></td>
		</tr>
		<tr>
			<td>event_name :</td>
			<td><form:input path="event_name" value="${eventname }"/></td>
		</tr>
		<tr>
			<td>customer_id :</td>
			<td><form:input path="customer_id" value="${userid}"/></td>
				
		</tr>

		<tr>
			<td>numberoftickets:</td>
			<td><form:input path="numberoftickets"  value="${tickets}"/></td>
			
		</tr>
		
		<tr>
			<td colspan="2"><input type="submit" value="Confirm"></td>
		</tr>
		
		
	</table>
</form:form>

<c:if test="${fn:length(bookingList) > 0}">
	<table cellpadding="5">
		<tr class="even">
			<th>booking_id</th>
			<th>numberoftickets</th>
			<th>event_id</th>
			
		</tr>
		<c:forEach items="${bookingList}" var="booking" varStatus="status">
			<tr class="<c:if test="${status.count % 2 == 0}">even</c:if>">
				<td>${booking.booking_id}</td>
				<td>${booking.numberoftickets}</td>
				<td>${booking.event_id}</td>
			
			</tr>
		</c:forEach>
	</table>
	
Now you can logout using the following link
<table><tr><td><a href="../customer/login.htm">LOGOUT</a></td></table>
	
</c:if>
</body>
</html>