<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>Login (CS) Page</title>
</head>
<body>

	
<table><tr><td><a href="../eventmanager/login.htm">Are you an Event Manager ? </a></td></tr>
<tr>
<td><a href="../customer/list.htm"> Not a Customer Yet ? </a></td></tr>
	</table>
<form:form action="verify.htm" commandName="customer">
	<table>
		<tr>
			<td>User Name :</td>
			<td><form:input path="name" /></td>
		</tr>
		<tr>
			<td>Password :</td>
			<td><form:password path="password" /></td>
		</tr>
			
			<td colspan="2"><input type="submit" value="Login"></td>
		</tr>
	</table>
</form:form>

	

</body>
</html>