<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.swen90007.example.domain.Register" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<%@ page isELIgnored="false" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>Tradesmen Home Page</title>
</head>
<body>


	<table>
		<tr> Welcome  <c:out	value="${name}" /> [Tradesmen] to Home page</tr>
		
		<% 

		Register res= (Register)request.getAttribute("request_user");	
		session.setAttribute("request_user",res );

		%>
		
		
		<tr>
		<td>
	To view the Bid jobs <a href="../bid/viewjobs.htm"> click here </a>
	</td>
	</tr>
	<tr>		

</tr>
	</table>
	
	
	<c:if test="${fn:length(joblist) > 0}">
	<table cellpadding="5">
		<tr class="even">
			<th>Job title: </th>
			<th>Job description:</th>
			<th>Max Price:</th>
			<th>Bid Price:</th>
		</tr>
		<c:forEach items="${joblist}" var="jobs" varStatus="status">
			<tr class="<c:if test="${status.count % 2 == 0}">even</c:if>">
				<td>${jobs.title}</td>
				<td>${jobs.description}</td>
				<td>${jobs.maxprice}</td>
				<td>${jobs.bidprice}</td>
			</tr>
		</c:forEach>
	</table>
</c:if>

</body>
</html>