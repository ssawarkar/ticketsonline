<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.swen90007.example.domain.Customer" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<%@ page isELIgnored="false" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>Bookings Page</title>
</head>
<body>


	<table>
		<% 
			Customer cs= (Customer)session.getAttribute("request_user");
		
		%>	
		
		<tr><td> Dear <% out.print(cs.getName());%> , you can now browse through the events and book tickets.</td></tr>
		<tr>
		<td>
	To view the Events <a href="../event/viewevents.htm"> click here </a>
	</td>
	</tr>
	<tr>		

</tr>
	</table>
	
	<c:if test="${fn:length(eventlist) > 0}">
	<table cellpadding="5">
		<tr class="even">
			<th>event_id </th>
			<th>user_id</th>
			<th>name</th>
			<th>description</th>
			<th>status</th>
			<th>posted_on</th>
			<th>event_date</th>
			<th>venue</th>
			<th>no of tickets</th>
			<th>Book here</th>
		</tr>
		<c:forEach items="${eventlist}" var="events" varStatus="status">
			<tr class="<c:if test="${status.count % 2 == 0}">even</c:if>">
				<td>${events.event_id}</td>
				<td>${events.user_id}</td>
				<td>${events.name}</td>
				<td>${events.description}</td>
				<td>${events.status}</td>
				<td>${events.posted_on}</td>
				<td>${events.event_date}</td>
				<td>${events.venue}</td>
				<td><input type="text" id="tickets" name="tickets" value="10"></td>
				
				  <script type="text/javascript"> 
				  		function link(e)
				  		{
				  		var numberoftickets =	document.getElementById("tickets").value;
				  		url ='../booking/send.htm?userid=<%= cs.getCustomer_id()%>&eventid=${events.event_id }&eventname=${events.name }&tickets='+numberoftickets 
				  		e.href= url;
				  		
				  		
				  		
				  		
				  		
				  		}
				  		</script>
				  		
				<td><a href="#null" onclick="link(this)">Book Now</a></td>
			</tr>
		</c:forEach>
	</table>
</c:if>

	
Now you can logout using the following link
<table><tr><td><a href="../customer/login.htm">LOGOUT</a></td></table>


</body>
</html>