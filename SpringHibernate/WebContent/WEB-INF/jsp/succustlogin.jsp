<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.swen90007.example.domain.EventManager" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>EventManager Home Page</title>
</head>
<body>

Dear   <c:out	value="${user.name}" /> , now you can create a new event by clicking the link below !!
	<table>
	
<% 

EventManager em= (EventManager)request.getAttribute("request_user");	
session.setAttribute("request_user",em );

%>


<tr> <td><a href="../event/list.htm">Create a New Event</a></td> </tr>



	</table>
	
	Now you can logout using the following link
<table><tr><td><a href="../customer/login.htm">LOGOUT</a></td></table>
	
</body>
</html>