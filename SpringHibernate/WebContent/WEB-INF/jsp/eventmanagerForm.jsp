<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>EventManager Registration Page</title>
</head>
<body>

	
<form:form action="add.htm" commandName="eventmanager">
	<table>
		<tr>
			<td>User Name :</td>
			<td><form:input path="name" /></td>
		</tr>
		<tr>
			<td>Password :</td>
			<td><form:password path="password" /></td>
		</tr>
		<tr>
			<td>Email ID :</td>
			<td><form:input path="email_id" /></td>
				
		</tr>
		<tr>
			<td>User Type :</td>
			<td><form:select path="user_type">
				<form:option value="0" label="Select" />
				<form:option value="1" label="Customer" />
				<form:option value="2" label="Event Manager" />
				
			</form:select></td>
		</tr>
	

		<tr>
			<td>Address:</td>
			<td><form:input path="address" /></td>
		</tr>
		<tr>
			<td>Passport ID :</td>
			<td><form:input path="passportid" /></td>
		</tr>
	
		<tr>
			<td>Phone No :</td>
			<td><form:input path="phone_no" /></td>
		</tr>
	
		<tr>
			<td colspan="2"><input type="submit" value="Register"></td>
		</tr>
	</table>
</form:form>
<c:if test="${fn:length(eventmanagerList) > 0}">
	<table cellpadding="5">
		<tr class="even">
			<th>Name</th>
			<th>Emailid</th>
			<th>phoneno</th>
			
		</tr>
		<c:forEach items="${eventmanagerList}" var="eventmanager" varStatus="status">
			<tr class="<c:if test="${status.count % 2 == 0}">even</c:if>">
				<td>${eventmanager.name}</td>
				<td>${eventmanager.email_id}</td>
				<td>${eventmanager.phone_no}</td>
			
			</tr>
		</c:forEach>
	</table>
	
	
</c:if>


After you register you can login into your account using the following link
<table><tr><td><a href="../eventmanager/login.htm">EventManager</a></td></table>
	
</body>
</html>