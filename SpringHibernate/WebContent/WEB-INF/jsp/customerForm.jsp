<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>Customer Registration Page</title>
</head>
<body>

	
<form:form action="add.htm" commandName="customer">
	<table>
		<tr>
			<td>User Name :</td>
			<td><form:input path="name" /></td>
		</tr>
		<tr>
			<td>Password :</td>
			<td><form:password path="password" /></td>
		</tr>
		<tr>
			<td>Email id :</td>
			<td><form:input path="email_id" /></td>
				
		</tr>
	

		<tr>
			<td>Address:</td>
			<td><form:input path="address" /></td>
			
		</tr>
		<tr>
			<td>Credit card type :</td>
			
			<td><form:input path="creditcardtype" /></td>
		</tr>
	
		<tr>
			<td>Credit card number :</td>
			<td><form:input path="creditcardnumber" /></td>
		</tr>
	
		<tr>
			<td>Passport ID :</td>
			<td><form:input path="passportid" /></td>
		</tr>
	
		<tr>
			<td>Phone_No :</td>
			<td><form:textarea path="phone_no" /></td>
		</tr>
	
		<tr>
			<td colspan="2"><input type="submit" value="Register"></td>
		</tr>
	</table>
</form:form>
<c:if test="${fn:length(customerList) > 0}">
	<table cellpadding="5">
		<tr class="even">
			<th>Name</th>
			<th>Email ID</th>
			<th>Phone No</th>
			
		</tr>
		<c:forEach items="${customerList}" var="customer" varStatus="status">
			<tr class="<c:if test="${status.count % 2 == 0}">even</c:if>">
				<td>${customer.name}</td>
				<td>${customer.email_id}</td>
				<td>${customer.phone_no}</td>
			
			</tr>
		</c:forEach>
	</table>
</c:if>

After you register you can login into your account using the following link
<table><tr><td><a href="../customer/login.htm">Customer</a></td></table>


</body>
</html>