<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>Add New Event</title>
</head>
<body>

	
<form:form action="add.htm" commandName="event">
	<table>
		<tr>
			<td>Event Name :</td>
			<td><form:input path="name" /></td>
		</tr>
		<tr>
			<td>Description :</td>
			<td><form:password path="description" /></td>
		</tr>
		<tr>
			<td>Status :</td>
			<td><form:input path="status" /></td>
				
		</tr>
		<tr>
			<td>Event Type :</td>
			<td><form:select path="user_id">
				<form:option value="0" label="Select" />
				<form:option value="1" label="Sports" />
				<form:option value="2" label="Concert" />
				<form:option value="3" label="Other" />
			</form:select></td>
		</tr>
	

		<tr>
			<td>Posted_On:</td>
			<td><form:textarea path="posted_on" /></td>
		</tr>
		<tr>
			<td>Event date :</td>
			<td><form:textarea path="event_date" /></td>
		</tr>
	
		<tr>
			<td>Venue :</td>
			<td><form:textarea path="venue" /></td>
		</tr>
	
		<tr>
			<td colspan="2"><input type="submit" value="Register"></td>
		</tr>
	</table>
</form:form>
<c:if test="${fn:length(eventList) > 0}">
	<table cellpadding="5">
		<tr class="even">
			<th>Name</th>
			<th>Venue</th>
			<th>event_date</th>
			
		</tr>
		<c:forEach items="${eventList}" var="event" varStatus="status">
			<tr class="<c:if test="${status.count % 2 == 0}">even</c:if>">
				<td>${event.name}</td>
				<td>${event.venue}</td>
				<td>${event.event_date}</td>
			
			</tr>
		</c:forEach>
	</table>
	
Now you can logout using the following link !
<table><tr><td><a href="../eventmanager/login.htm">LOGOUT</a></td></table>
	
</c:if>
</body>
</html>