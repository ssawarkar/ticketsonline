package com.swen90007.example.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BOOKING")
public class Booking {

	private Long booking_id;	
	private Long event_id;
	private String event_name;
	private Long customer_id;	
	private Long numberoftickets;


	@Id
	@GeneratedValue
	@Column(name="BOOKING_ID")
	public Long getBooking_id() {
		return booking_id;
	}
	public void setBooking_id(Long booking_id) {
		this.booking_id = booking_id;
	}
	
	@Column(name="EVENT_ID")
	public Long getEvent_id() {
		return event_id;
	}
	public void setEvent_id(Long event_id) {
		this.event_id = event_id;
	}
	
	@Column(name="EVENT_NAME")
		public String getEvent_name() {
		return event_name;
	}
	public void setEvent_name(String event_name) {
		this.event_name= event_name;
	}
	
	@Column(name="CUSTOMER_ID")
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	
	@Column(name="NUMBEROFTICKETS")
	public Long getNumberoftickets() {
		return numberoftickets;
	}
	public void setNumberoftickets(Long numberoftickets) {
		this.numberoftickets = numberoftickets;
	}
	
}
