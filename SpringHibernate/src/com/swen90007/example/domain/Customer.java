package com.swen90007.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CUSTOMER_NEW")
public class Customer {

	private Long customer_id;
	private Long user_id;
	private String name;
	private String password;
	private String email_id;
	private String user_type;
	private String address;
	private String creditcardtype;
	private String creditcardnumber;
	private String creditcardname;
	private String passportid;
	private String phone_no;	
	
	
	
	@Id
	@GeneratedValue
	@Column(name="CUSTOMER_ID")
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	
	@Column(name="NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="PASSWORD")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="EMAIL_ID")
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	
	@Column(name="USER_TYPE")
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	
	@Column(name="ADDRESS")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name="CREDITCARDTYPE")
	public String getCreditcardtype() {
		return creditcardtype;
	}
	public void setCreditcardtype(String creditcardtype) {
		this.creditcardtype = creditcardtype;
	}
	
	@Column(name="CREDITCARDNUMBER")
	public String getCreditcardnumber() {
		return creditcardnumber;
	}
	public void setCreditcardnumber(String creditcardnumber) {
		this.creditcardnumber = creditcardnumber;
	}

	@Column(name="CREDITCARDNAME")
	public String getCreditcardname() {
		return creditcardname;
	}
	public void setCreditcardname(String creditcardname) {
		this.creditcardname = creditcardname;
	}
	
	@Column(name="PASSPORTID")
	public String getPassportid() {
		return passportid;
	}
	public void setPassportid(String passportid) {
		this.passportid = passportid;
	}

	
	@Column(name="PHONE_NO")
	public String getPhone_no() {
		return phone_no;
	}
	public void setPhone_no(String phone_no) {
		this.phone_no= phone_no;
	}

		
	
}
