package com.swen90007.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EVENT_NEW")
public class Event {

	private Long event_id;
	private Long user_id;
	private String name;
	private String description;
	private int status;
	private String posted_on;
	private String event_date;
	private String venue;
	
	
	@Id
	@GeneratedValue
	@Column(name="EVENT_ID")
	public Long getEvent_id() {
		return event_id;
	}
	public void setEvent_id(Long event_id) {
		this.event_id = event_id;
	}
	
	@Column(name="NAME")
		public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="USER_ID")
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	
	@Column(name="STATUS")
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	@Column(name="POSTED_ON")
	public String getPosted_on() {
		return posted_on;
	}
	public void setPosted_on(String posted_on) {
		this.posted_on = posted_on;
	}
	
	@Column(name="VENUE")
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	
	@Column(name="EVENT_DATE")
	public String getEvent_date() {
		return event_date;
	}
	public void setEvent_date(String event_date) {
		this.event_date = event_date;
	}
}
