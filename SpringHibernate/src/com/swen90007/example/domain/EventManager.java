package com.swen90007.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EVENTMANAGER_NEW")
public class EventManager {

	private Long eventmanager_id;
	private Long user_id;
	private String name;
	private String password;
	private String email_id;
	private String user_type;
	private String address;
	private String passportid;
	private String phone_no;	
	
	
	
	@Id
	@GeneratedValue
	@Column(name="EVENTMANAGER_ID")
	public Long getEventManager_id() {
		return eventmanager_id;
	}
	public void setEventManager_id(Long eventmanager_id) {
		this.eventmanager_id = eventmanager_id;
	}
	
	@Column(name="NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="PASSWORD")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="EMAIL_ID")
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	
	@Column(name="USER_TYPE")
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	
	@Column(name="ADDRESS")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	

	@Column(name="PASSPORTID")
	public String getPassportid() {
		return passportid;
	}
	public void setPassportid(String passportid) {
		this.passportid = passportid;
	}

	
	@Column(name="PHONE_NO")
	public String getPhone_no() {
		return phone_no;
	}
	public void setPhone_no(String phone_no) {
		this.phone_no= phone_no;
	}

		
	
}
