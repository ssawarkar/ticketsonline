package com.swen90007.example.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.swen90007.example.dao.BookingDAO;
import com.swen90007.example.dao.EventDAO;
import com.swen90007.example.dao.EventDAOImpl;
import com.swen90007.example.domain.Booking;
import com.swen90007.example.domain.Event;

public class BookingController extends MultiActionController {

	private BookingDAO bookingDAO;
	private	EventDAO eventDAO;

	public void setBookingDAO(BookingDAO bookingDAO) {
		this.bookingDAO = bookingDAO;
	}
	
	public void setEventDAO(EventDAO eventDAO)
	{
		this.eventDAO=eventDAO;
	}

	public ModelAndView add(HttpServletRequest request,
			HttpServletResponse response, Booking booking) throws Exception {
		
	
		

		System.out.println("Event Id is: "+ booking.getEvent_id());
		List<Event>  e=eventDAO.searchEventBooking(booking.getEvent_id());
		
		int e_getStatus =e.get(0).getStatus(); 
		if ( e_getStatus>0)
		{
			if (e_getStatus - booking.getNumberoftickets() <0)
			{
					System.out.println("No enough Tickets available");
			}

			else
			{
				bookingDAO.saveBooking(booking);

				e.get(0).setStatus( (int) ( e_getStatus - booking.getNumberoftickets()));
				eventDAO.saveEvent(e.get(0));
					
			}
			
		}
		return new ModelAndView("redirect:list.htm?");
	}


	//url ='../booking/add.htm?userid=<%= cs.getCustomer_id()%>&eventid=${events.event_id }&eventname=${events.event_name }&tickets='+numberoftickets;
	
	public ModelAndView list(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("bookingList", bookingDAO.listBooking());
		modelMap.addAttribute("booking", new Booking());
		return new ModelAndView("booking", modelMap);
	
		
	}
	
	
	public ModelAndView send(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String userid = request.getParameter("userid");
		String eventid = request.getParameter("eventid");
		String eventname = request.getParameter("eventname");
		String tickets = request.getParameter("tickets");
		
		
		
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("userid", userid);
		modelMap.addAttribute("eventid", eventid);
		modelMap.addAttribute("eventname", eventname);
		modelMap.addAttribute("tickets", tickets);
		modelMap.addAttribute("booking", new Booking());
		return new ModelAndView("booking", modelMap);
	
		
	}
	
}
