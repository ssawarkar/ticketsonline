package com.swen90007.example.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.swen90007.example.dao.CustomerDAO;
import com.swen90007.example.dao.EventManagerDAO;
import com.swen90007.example.domain.Customer;
import com.swen90007.example.domain.EventManager;

public class CustomerController extends MultiActionController {

	private CustomerDAO customerDAO;
	private String name;
	private Long user_id;
	private String password;
	private Customer cs;

	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	public ModelAndView add(HttpServletRequest request,
			HttpServletResponse response, Customer customer) throws Exception {
		customerDAO.saveCustomer(customer);
		return new ModelAndView("redirect:list.htm");
	}

	public ModelAndView list(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("customerList", customerDAO.listCustomer());
		modelMap.addAttribute("customer", new Customer());
		return new ModelAndView("customerForm", modelMap);
	}
	
	
	
	public ModelAndView login(HttpServletRequest request,
			HttpServletResponse response, Customer customer) throws Exception {
		return new ModelAndView("redirect:loginCustomer.htm");
	}
	
	public ModelAndView loginCustomer(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("customerList", customerDAO.listCustomer());
		modelMap.addAttribute("customer", new Customer());
		return new ModelAndView("loginCustomer", modelMap);
	}
	
	
	
	public ModelAndView verify(HttpServletRequest request,
			HttpServletResponse response, Customer customer) throws Exception {

	List<Customer> customers =	customerDAO.searchRegister(customer.getName(),customer.getPassword());
	System.out.println("Elements in the  Verify list are"+customers.toString());
	
	System.out.println("Value at customerss"+ customers.get(0));
	
	name=customers.get(0).getName().toString();
	password=customers.get(0).getPassword().toString();
	user_id=customers.get(0).getCustomer_id();
	cs=customers.get(0);
	

	if(customers.size()>0)
		{
				return new ModelAndView("redirect:successEventManagerLogin.htm");
		}	
			else
			{
				 return new ModelAndView("redirect:errorlogin.htm");
			}
		
	}
	
	
	
	
	public ModelAndView successEventManagerLogin(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("customerList", customerDAO.listCustomer());
		modelMap.addAttribute("customer", new Customer());
		modelMap.addAttribute("name",name);
		modelMap.addAttribute("user_id",user_id);
		modelMap.addAttribute("user",cs);
		request.setAttribute("request_user", cs);
		return new ModelAndView("successEventManagerLogin", modelMap);
	}

}
