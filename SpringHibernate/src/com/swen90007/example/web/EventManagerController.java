package com.swen90007.example.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;


import com.swen90007.example.dao.EventManagerDAO;
import com.swen90007.example.domain.EventManager;


public class EventManagerController extends MultiActionController {

	private EventManagerDAO eventmanagerDAO;
	private String name;
	private Long user_id;
	private String password;
	private EventManager em;
	
	
	public void setEventManagerDAO(EventManagerDAO eventmanagerDAO) {
		this.eventmanagerDAO = eventmanagerDAO;
	}

	public ModelAndView add(HttpServletRequest request,
			HttpServletResponse response, EventManager eventmanager) throws Exception {
		eventmanagerDAO.saveEventManager(eventmanager);
		return new ModelAndView("redirect:list.htm");
	}

	public ModelAndView list(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("eventmanagerList", eventmanagerDAO.listEventManager());
		modelMap.addAttribute("eventmanager", new EventManager());
		return new ModelAndView("eventmanagerForm", modelMap);
	}
	
	public ModelAndView login(HttpServletRequest request,
			HttpServletResponse response, EventManager eventmanager) throws Exception {
		return new ModelAndView("redirect:loginEventManager.htm");
	}
	
	public ModelAndView loginEventManager(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("eventmanagerList", eventmanagerDAO.listEventManager());
		modelMap.addAttribute("eventmanager", new EventManager());
		return new ModelAndView("loginEventManager", modelMap);
	}
	
	
	
	public ModelAndView verify(HttpServletRequest request,
			HttpServletResponse response, EventManager eventmanager) throws Exception {

	List<EventManager> eventmanagers =	eventmanagerDAO.searchRegister(eventmanager.getName(),eventmanager.getPassword());
	System.out.println("Elements in the  Verify list are"+eventmanagers.toString());
	
	System.out.println("Value at eventmanagers"+ eventmanagers.get(0));
	
	name=eventmanagers.get(0).getName().toString();
	password=eventmanagers.get(0).getPassword().toString();
	user_id=eventmanagers.get(0).getEventManager_id();
	em=eventmanagers.get(0);
	

	if(eventmanagers.size()>0)
		{
				return new ModelAndView("redirect:successcustlogin.htm");
		}	
			else
			{
				 return new ModelAndView("redirect:errorlogin.htm");
			}
		
	}
	
	
	
	
	public ModelAndView successcustlogin(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("eventmanagerList", eventmanagerDAO.listEventManager());
		modelMap.addAttribute("eventmanager", new EventManager());
		modelMap.addAttribute("name",name);
		modelMap.addAttribute("user_id",user_id);
		modelMap.addAttribute("user",em);
		request.setAttribute("request_user", em);
		return new ModelAndView("succustlogin", modelMap);
	}
	
	
	
}
