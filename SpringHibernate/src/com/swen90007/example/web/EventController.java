package com.swen90007.example.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.swen90007.example.dao.EventDAO;

import com.swen90007.example.domain.Event;

public class EventController extends MultiActionController {

	private EventDAO eventDAO;

	public void setEventDAO(EventDAO eventDAO) {
		this.eventDAO = eventDAO;
	}

	public ModelAndView add(HttpServletRequest request,
			HttpServletResponse response, Event event) throws Exception {
		eventDAO.saveEvent(event);
		return new ModelAndView("redirect:list.htm");
	}

	public ModelAndView list(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("eventList", eventDAO.listEvent());
		modelMap.addAttribute("event", new Event());
		return new ModelAndView("eventForm", modelMap);
	}
	


	public ModelAndView viewevents(HttpServletRequest request,
			HttpServletResponse response, Event event) throws Exception {
		
		ModelMap modelMap =new ModelMap();
		modelMap.addAttribute("eventlist", eventDAO.searchEvent());
		modelMap.addAttribute("event", new  Event());
		return new ModelAndView("viewevents",modelMap);
	}

	public Event searchevent(Long event_id) {
		// TODO Auto-generated method stub
		List<Event> events=	eventDAO.searchEventBooking(event_id);
		return events.get(0);
	}

}
