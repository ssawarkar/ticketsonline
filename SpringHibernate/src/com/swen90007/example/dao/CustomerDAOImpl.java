package com.swen90007.example.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateTemplate;
import com.swen90007.example.domain.*;

public class CustomerDAOImpl implements CustomerDAO {

	private HibernateTemplate hibernateTemplate;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void saveCustomer(Customer customer) {

		Session session = hibernateTemplate.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(customer);
		transaction.commit();
		session.close();
		
	}

	
	@Override
	public List<Customer> searchRegister(String username,String password ) {
		
		return hibernateTemplate.findByNamedParam("From Customer WHERE name=:username AND password=:password", new String[]{"username","password"}, new Object[]{username,password});
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public boolean authenticateCustomer(String username,String password) {
		@SuppressWarnings("unchecked")
		List<Customer> customers  = hibernateTemplate.find("from Customer where name = ?", username);
		for(Customer customer : customers){
		    // you probably don't need the id, so I'll skip that
		    //String email = customer.getName();
		    String user = customer.getPassword();
		    if (username == user)
		    {
		    	return true;
		    }
		 
		}
		return false;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Customer> listCustomer() {
		return hibernateTemplate.find("from Customer");
	}

}
