package com.swen90007.example.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateTemplate;
import com.swen90007.example.domain.*;

public class EventDAOImpl implements EventDAO {

	private HibernateTemplate hibernateTemplate;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void saveEvent(Event event) {

		Session session = hibernateTemplate.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(event);
		transaction.commit();
		session.close();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Event> listEvent() {
		return hibernateTemplate.find("from Event");
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<Event> searchEvent() {
		String a="A";
		return hibernateTemplate.find("from Event ");
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override

	public List<Event> searchEventBooking(Long event_id) {
		// TODO Auto-generated method stub

		return hibernateTemplate.findByNamedParam("From Event where event_id=:event_id ",new String[] {"event_id"},new  Object[] {event_id});    
	}

	
}
