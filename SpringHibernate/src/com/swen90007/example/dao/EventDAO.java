package com.swen90007.example.dao;

import java.util.List;


import com.swen90007.example.domain.Event;


public interface EventDAO {
	
	public void saveEvent(Event event) ;
	public List<Event> listEvent() ;
	public List<Event> searchEvent();
	public List<Event> searchEventBooking(Long event_id);
	
	
}
