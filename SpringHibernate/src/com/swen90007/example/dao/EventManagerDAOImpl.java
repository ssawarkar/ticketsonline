package com.swen90007.example.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateTemplate;
import com.swen90007.example.domain.*;

public class EventManagerDAOImpl implements EventManagerDAO {

	private HibernateTemplate hibernateTemplate;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void saveEventManager(EventManager eventmanager) {

		Session session = hibernateTemplate.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(eventmanager);
		transaction.commit();
		session.close();
		
	}
	

	@Override
	public List<EventManager> searchRegister(String username,String password ) {
		
		return hibernateTemplate.findByNamedParam("From EventManager WHERE name=:username AND password=:password", new String[]{"username","password"}, new Object[]{username,password});
		// TODO Auto-generated method stub
		
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public List<EventManager> listEventManager() {
		return hibernateTemplate.find("from EventManager");
	}

}
