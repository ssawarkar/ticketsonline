package com.swen90007.example.dao;

import java.util.List;

import com.swen90007.example.domain.Booking;


public interface BookingDAO {
	
	public  void saveBooking(Booking booking) ;
	public List<Booking> listBooking() ;
	public List<Booking> searchBooking();
//	public Object makeBooking(Booking booking, Long customer_id,Long event_id,String event_name,Long numberoftickets);
	
}
