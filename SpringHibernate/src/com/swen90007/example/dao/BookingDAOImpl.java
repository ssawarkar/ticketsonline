package com.swen90007.example.dao;


import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateTemplate;
import com.swen90007.example.domain.*;

public class BookingDAOImpl implements BookingDAO {

	private HibernateTemplate hibernateTemplate;
	private Date date;
	

	public void setSessionBookingFactory(SessionFactory sessionBookingFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionBookingFactory);
	}

	@Override
	public  void saveBooking(Booking booking) {
	
		
		Session session = hibernateTemplate.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(booking);
		transaction.commit();
		session.close();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Booking> listBooking() {
		return hibernateTemplate.find("from Booking");
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Booking> searchBooking() {
		String a="A";
		return hibernateTemplate.find("from Booking ");
		// TODO Auto-generated method stub
		
	}

	/*
	
	@Override
	public Object makeBooking(Booking booking, Long customer_id, Long event_id,String event_name,Long numberoftickets) {
		Long booking_id= booking.getBooking_id();
		List<Booking> bookings= hibernateTemplate.findByNamedParam("From Booking WHERE booking_id=:booking_id", new String[]{"jobid"}, new Object[]{bookingid});
		Booking booking= booking.get(0);
		System.out.println("Fetched bid from the data base is "+ jobs.get(0).getTitle().toString());
		if(job.getStatus().toString().equalsIgnoreCase("A") || job.getStatus().toString().equalsIgnoreCase("B"))
		{
			
			System.out.println("I am in Big if in make bid");
			System.out.println("Original Bid price is: "+ job.getBidprice());
			System.out.println("New bid Price is: "+ newprice);
			
			if(job.getBidprice()> newprice )
			{
				job.setBidprice(newprice);
				job.setTradeid(tradeid);
				job.setBidby(tradeName);
				job.setStatus("B");
				hibernateTemplate.saveOrUpdate(job);
				return("Bid accepted");
				
			}
			else
			{
				return("Bid less than already bid price");
			}
			
		}
		else
				return("Job not available");
		
	// TODO Auto-generated method stub
		//return null;
	
	
	}
	 */
	
	
}

