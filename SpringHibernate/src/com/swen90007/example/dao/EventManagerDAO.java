package com.swen90007.example.dao;

import java.util.List;
import com.swen90007.example.domain.EventManager;


public interface EventManagerDAO {
	
	public void saveEventManager(EventManager eventmanager) ;
	public List<EventManager> listEventManager() ;
	public List<EventManager> searchRegister(String name, String password);
	
}
