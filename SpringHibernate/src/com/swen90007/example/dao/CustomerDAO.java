package com.swen90007.example.dao;

import java.util.List;
import com.swen90007.example.domain.Customer;


public interface CustomerDAO {
	
	public void saveCustomer(Customer customer) ;
	public boolean authenticateCustomer(String username,String password) ;
	public List<Customer> listCustomer() ;
	public List<Customer> searchRegister(String name, String password);
}
