# README #


### What is this repository for? ###

* SDA project
* Suraj (619308) and Jaswanth (616536)

### How do I get set up? ###
* Eclipse Indigo (Java EE)  
* Spring Framework Integrated
* Hibernate Framework Integrated
* All the other configuration mentioned in the workshops should be followed
* Derby Database Server running
* Refer the Submission 3 Document to know the activity flow of our project.

### Who do I talk to? ###

* Jaswanth Manigundan 
* (jmanigundan@student.unimelb.edu.au)
* Suraj Sawarkar 
* (ssawarkar@student.unimelb.edu.au)